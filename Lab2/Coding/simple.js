//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let array=[54,-16,80,55,-74,73,26,5,-34,-73,19,63,-55,-61,65,-14,-19,-51,-17,-25];
let pOdd=[];//leave empty
let nEven=[]; //leave empty
for (let i=0; i<array.length; i++){
  if (array[i]>=0 && Math.abs(array[i]%2)!==0){
    pOdd.push(array[i])
  }else if(array[i]<0 && Math.abs(array[i]%2)===0){
    nEven.push(array[i])
  }else{}
}
    output='Positive Odd:' + pOdd + '\n' + 'Negative Even' + nEven + '\n'
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 

    //Question 2 here 
let one=0;let two=0;let three=0; let four=0; let five=0;let six=0;
let die
for (let i=0; i<60000; i++){
  die=Math.floor((Math.random() * 6) + 1);
  if (die===1){
    one++
  }else if(die===2){
    two++
  }else if(die===3){
    three++
  }else if(die===4){
    four++
  }else if(die===5){
    five++
  }else if(die===6){
    six++
  }else{
    
  }
}
output='Frequency of die rolls' + '\n' + '1:' + one + '\n' + '2:' + two + '\n' + '3:' + three + '\n' + '4:' + four + '\n' + '5:' + five + '\n' + '6:' + six;
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    let freq=[0,0,0,0,0,0,0];
let die
for (let i=1; i<=60000; i++){
  die=Math.floor((Math.random() * 6) + 1);
  freq[die]++
}
  output='Frequency of die rolls' + '\n'
for (let j=1; j<freq.length; j++){
  output+= j + ':' + freq[j] + '\n'
}
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    let dieRolls={
  freq:{
  1:0,
  2:0,
  3:0,
  4:0,
  5:0,
  6:0
  },
   total:60000,
   excepetions:''
}
let die
for (i=0; i<dieRolls.total; i++){
  die=Math.floor((Math.random() * 6) + 1);
  dieRolls.freq[die]++
}
 output='Frequency of dice rolls' + '\n' + 'Total rolls :' + dieRolls.total + '\n' + 'Frequency:' + '\n';
for (let prop in dieRolls.freq){
  output+= prop + ':' + dieRolls.freq[prop] + '\n';
  if (Math.abs(dieRolls.freq[prop]-(dieRolls.total)/6)>=0.01*(dieRolls.total)/6 ) {
    dieRolls.excepetions+=prop + ' '
  }
}
output+= 'Excepetions:' + dieRolls.excepetions
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    let person = {
name: "Jane",
 income: 127050
}
let tax
if (person.income<=18200){
  tax=0
}else if(person.income>18200 && person.income<=37000){
  tax=0.19*(person.income-18200)
}else if(person.income>37000 && person.income<=90000){
  tax=3572+0.325*(person.imcome-37000)
}else if(person.income>90000 && person.income<=180000){
  tax=20797+0.37*(person.income-90000)
}else if(person.income>180000){
  tax=54097+0.45*person.income
}else{
  tax='Error'
}
output=person.name + '\'s income is : $' + person.income + ', and her tax owed is : $' + tax;

    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}