// question 3
/*
Firstly take the first number in an array as current minimum and current maximum
Secondly using for loop take each number in an array and using if statmen to check whether
the current number is larger than current max or smaller than current min, if so, the current max or 
current min is replaced by current number
Finally, after all numbers are checked, the max and min are returned
*/

// question 3
function extremeValues(array){
  let currentMin=array[0]
  let currentMax=array[0]
  let result=[]
  for (let i=1; i<array.length; i++){
    if (array[i] <= currentMin){
      currentMin=array[i]
    }else if(array[i] > currentMax){
      currentMax=array[i]
    }else{}
  }
    result.push(currentMax,currentMin)
  return result
}

var values = [4, 3, 6, 12, 1, 3, 8];
//console.log(extremeValues(values))
let output4
output4=extremeValues(values)
let outputArea4=document.getElementById('outputArea3');
outputArea4.innerText=output4