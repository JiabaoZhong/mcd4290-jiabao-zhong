//question 1
function objectToHTML(object){
  let results=''
  for (let prop in object){
    results+= prop + ' : ' + object[prop] + '\n'
  }
  return results
}
var testObj = {
 number: 1,
 string: "abc",
 array: [5, 4, 3, 2, 1],
 boolean: true
};
let output
output=objectToHTML(testObj)
//console.log(objectToHTML(testObj))
let outputArea=document.getElementById('outputArea1');
outputArea.innerText=output

